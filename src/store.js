import {createStore, applyMiddleware, combineReducers} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import form from './reducers/form';

const Config = process.env.NODE_ENV;

const getStore = () => {
  if (Config === 'development') {
    const store = createStore(
      combineReducers({
        form,
      }),
      applyMiddleware(thunk),
      applyMiddleware(logger)
    );
    return store;
  } else if (Config === 'production') {
    const store = createStore(combineReducers({
      form,
    }),
      applyMiddleware(thunk)
    );
    return store;
  }
};

export default getStore;
