import axios from "axios";
import { config } from '../config/config';

let forecastAPi = config.apiBase + '/forecast'

function getForecast(id){
    return axios.get(forecastAPi,{
        params: {
            id:id,
            appid:config.openWeatherKey,
            units:'metric'
          }
    })
}

function getForecastByName(name){
    return axios.get(forecastAPi,{
        params: {
            q:name,
            appid:config.openWeatherKey,
            units:'metric'
          }
    })
}


export {
    getForecast,
    getForecastByName
}